import json

from utils import fetch_json_data
from date_selection import args, from_date, to_date

cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list = [[] for _ in range(3)]

cust_to_onep_rzp_get_url = "https://api.pharmacyone.io/prod/rzp_transaction"
cust_to_onep_cf_post_url = "https://api.pharmacyone.io/prod/cf_payments"
onep_to_ret_rzp_get_url = "https://api.pharmacyone.io/prod/rzp_payout"

onep_to_ret_rzp_get_headers, cust_to_onep_rzp_get_headers = [{"session-token": "wantednote"} for _ in range(2)]
cust_to_onep_cf_post_headers = {"session-token": "wantednote", "Content-Type": "application/json"}


cust_to_onep_cf_body = json.dumps({"startDate": from_date, "endDate": to_date})
cust_to_onep_rzp_body, onep_to_ret_rzp_body = [None for _ in range(2)]

# Fetching json data
cust_to_onep_rzp_list = fetch_json_data(cust_to_onep_rzp_get_url, cust_to_onep_rzp_get_headers, cust_to_onep_rzp_body, from_date, to_date)
print("len(cust_to_onep_rzp_list): ", len(cust_to_onep_rzp_list))

cust_to_onep_cf_list = fetch_json_data(cust_to_onep_cf_post_url, cust_to_onep_cf_post_headers, cust_to_onep_cf_body, from_date, to_date)
print("len(cust_to_onep_cf_list): ", len(cust_to_onep_cf_list))

onep_to_ret_rzp_list = fetch_json_data(onep_to_ret_rzp_get_url, onep_to_ret_rzp_get_headers, onep_to_ret_rzp_body, from_date, to_date )
print("len(onep_to_ret_rzp_list): ", len(onep_to_ret_rzp_list))

