from openpyxl import load_workbook

from date_selection import args
from matching_ids import total_transaction_value, pay_in_list, pay_out_list, utr_list, amount_list, cid_list, cname_list, created_at_list, unmatched_cust_to_onep_rzp_payin_list, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2
from matching_ids import unmatched_cust_to_onep_cf_payin_list, unmatched_onep_to_ret_payout_list, unmatched_utr_list, unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list, settlement_ids_list_sheeet_2

# Writing to excel sheet
workbook = load_workbook("rzp_cf.xlsx")

summary_sheet = workbook["summary"]
summary_sheet.append([f"April{args.day}", total_transaction_value])

matched_info_sheet = workbook.create_sheet(f"April{args.day}")
unmatched_info_sheet = workbook.create_sheet(f"April{args.day}(unmatched)")

matched_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "cname", "created_at", "settlement_id", "settlement_amount"])
unmatched_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "cname", "created_at"])

for i in range(len(pay_in_list)):
    matched_info_sheet.append([pay_in_list[i], pay_out_list[i], utr_list[i], amount_list[i], cid_list[i], cname_list[i], created_at_list[i], settlement_ids_list_sheeet_2[i]])

for i in range(len(unmatched_cust_to_onep_rzp_payin_list)):
    unmatched_info_sheet.append([unmatched_cust_to_onep_rzp_payin_list[i], "", "", unmatched_amount_list_2[i], unmatched_cid_list_2[i], "", unmatched_created_at_list_2[i]])

for i in range(len(unmatched_cust_to_onep_cf_payin_list)):
    unmatched_info_sheet.append([unmatched_cust_to_onep_cf_payin_list[i], "", "", "", "", "", ""])

for i in range(len(unmatched_onep_to_ret_payout_list)):
    unmatched_info_sheet.append(["", unmatched_onep_to_ret_payout_list[i], unmatched_utr_list[i], unmatched_amount_list[i], unmatched_cid_list[i], "", unmatched_created_at_list[i]])

workbook.save("rzp_cf.xlsx")
