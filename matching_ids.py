import copy

from fetching_json_data import cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list
from utils import unix_timestamp_to_date, removing_duplicates,fetch_json_data, extract_company_name, settlement_api, extract_company_name
from date_selection import args

final_settlement_ids_list, not_settlement_type_list, cust_to_onep_rzp_payin_list_ids, cust_to_onep_rzp_list_2, onep_to_ret_rzp_payout_list_ids, onep_to_ret_rzp_list_2 = [[] for _ in range(6)]
matching_count, total_transaction_value = 0, 0
utr_list, pay_out_list, pay_in_list, amount_list, cid_list, created_at_list, unmatched_cust_to_onep_cf_payin_list = [[] for _ in range(7)]
settlement_ids_list_sheet_list, final_settlement_ids_list_sheet_list, settlement_amount_sheet_2, settlement_ids_list_sheeet_2, settlement_ids_list_sheet, settlement_amount_sheet, cname_list = [[] for _ in range(7)]
unmatched_utr_list, unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2 = [[] for i in range(7)]
settlement_amount_list_sheeet_2 = []
# Getting settlement ids
for i in onep_to_ret_rzp_list:
    try:
        if i.get("source").get("notes").get("type") == "settlement":
            settlement_ids_list = settlement_api(i.get("source").get("notes").get("id"))
            for item in settlement_ids_list:
                final_settlement_ids_list.append(item)
    except AttributeError:
        pass

unmatched_cust_to_onep_cf_list = copy.deepcopy(cust_to_onep_cf_list)

# Matching cashfree reference ids with pay_out {onep_to_ret_rzp_list}
for i in final_settlement_ids_list:
    for j in cust_to_onep_cf_list:
        if i.get("id") == str(j.get("referenceId")):
            unmatched_cust_to_onep_cf_list.remove(j)
            matching_count += 1
            for item in onep_to_ret_rzp_list:
                try:
                    if item.get("source").get("notes").get("type") == "settlement":
                        if item.get("source").get("notes").get("id") not in settlement_ids_list_sheeet_2:
                            settlement_ids_list_sheeet_2.append(item.get("source").get("notes").get("id"))
                    if str(j.get("referenceId")) == item.get("source").get("notes").get("id"):
                        if item.get("source").get("notes").get("type") == "settlement":
                            pay_in_list.append(i.get("id"))
                            pay_out_list.append(j.get("referenceId"))
                            utr_list.append(item.get("source").get("utr"))
                            amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                            cid_list.append(item.get("source").get("notes").get("cid"))
                            created_at_list.append(item.get("created_at"))
                            total_transaction_value += item.get("amount")

                        if item.get("source").get("notes").get("type") != "settlement":
                            pay_in_list.append(i.get("id"))
                            pay_out_list.append(j.get("referenceId"))
                            utr_list.append(item.get("source").get("utr"))
                            amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                            cid_list.append(item.get("source").get("notes").get("cid"))
                            created_at_list.append(item.get("created_at"))
                            total_transaction_value += item.get("amount")
                            settlement_ids_list_sheeet_2.append("")
                except AttributeError:
                    pass                    

# Getting umnatched cashfree ids
for i in unmatched_cust_to_onep_cf_list:
    unmatched_cust_to_onep_cf_payin_list.append(str(i.get("referenceId")))

# Adding only valid ids to corresponding lists
for item in cust_to_onep_rzp_list:
    try:
        if item.get("id"):
            cust_to_onep_rzp_payin_list_ids.append(item.get("id"))
            cust_to_onep_rzp_list_2.append(item)
    except AttributeError:
        pass

for item in onep_to_ret_rzp_list:
    try:
        if (item.get("source").get("notes").get("id") and item.get("source").get("status") == "processed"):
            onep_to_ret_rzp_payout_list_ids.append(item.get("source").get("notes").get("id"))
            onep_to_ret_rzp_list_2.append(item)
    except AttributeError:
        pass  

# Removing duplicates from payin and payout ids
cust_to_onep_rzp_payin_list_ids = removing_duplicates(cust_to_onep_rzp_payin_list_ids)
onep_to_ret_rzp_payout_list_ids = removing_duplicates(onep_to_ret_rzp_payout_list_ids)

unmatched_cust_to_onep_rzp_payin_list = copy.deepcopy(cust_to_onep_rzp_payin_list_ids)
unmatched_onep_to_ret_payout_list = copy.deepcopy(onep_to_ret_rzp_payout_list_ids)

# Matching razorpay pay_in ids with pay_out {onep_to_ret_rzp_list}
for item_01 in cust_to_onep_rzp_payin_list_ids:
    if item_01 in onep_to_ret_rzp_payout_list_ids:
        matching_count += 1

        unmatched_cust_to_onep_rzp_payin_list.remove(item_01)
        unmatched_onep_to_ret_payout_list.remove(item_01)

        for item in onep_to_ret_rzp_list:
            try:
                if item.get("source").get("notes").get("type") == "settlement":
                    if item.get("source").get("notes").get("id") not in settlement_ids_list_sheeet_2:
                        settlement_ids_list_sheeet_2.append(item.get("source").get("notes").get("id"))
                if item_01 == item.get("source").get("notes").get("id"):
                    if item.get("source").get("notes").get("type") == "settlement":
                        pay_in_list.append(item_01)
                        pay_out_list.append(item_01)
                        utr_list.append(item.get("source").get("utr"))
                        amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                        cid_list.append(item.get("source").get("notes").get("cid"))
                        created_at_list.append(item.get("created_at"))
                        total_transaction_value += item.get("amount")
                    if item.get("source").get("notes").get("type") != "settlement":
                            pay_in_list.append(item_01)
                            pay_out_list.append(item_01)
                            utr_list.append(item.get("source").get("utr"))
                            amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                            cid_list.append(item.get("source").get("notes").get("cid"))
                            created_at_list.append(item.get("created_at"))
                            total_transaction_value += item.get("amount")
                            settlement_ids_list_sheeet_2.append("")
            except AttributeError:
                pass
print(len(pay_in_list), len(pay_out_list), len(utr_list), len(amount_list), len(cid_list), len(created_at_list), len(settlement_ids_list_sheet), len(settlement_amount_sheet))

count = 0

for i in settlement_ids_list_sheeet_2:
    if i != "":
        count += 1

for i in range(count):
    settlement_ids_list_sheeet_2.remove("")

print(settlement_ids_list_sheeet_2)
print("len(settlement_ids_list_sheeet_2)", len(settlement_ids_list_sheeet_2))

cname_list = extract_company_name(cid_list)

print("matching_count: ", matching_count)
print("total_transaction_value (in Rupees): ", int(total_transaction_value/ 100))

# Extracting the unmatched_ids information
for item_01 in unmatched_onep_to_ret_payout_list:
    for item in onep_to_ret_rzp_list_2:
        if item_01 == item.get("source").get("notes").get("id"):
            try:
                unmatched_utr_list.append(item.get("source").get("utr"))
                unmatched_amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                unmatched_cid_list.append(item.get("source").get("notes").get("cid"))
                unmatched_created_at_list.append(item.get("created_at"))
            except AttributeError:
                pass

for item_01 in unmatched_cust_to_onep_rzp_payin_list:
    for item in cust_to_onep_rzp_list_2:
        if item_01 == item.get("id"):
            try:
                unmatched_amount_list_2.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                unmatched_cid_list_2.append(item.get("notes").get("cid"))
                unmatched_created_at_list_2.append(item.get("created_at"))
            except AttributeError:
                pass

for item_01 in unmatched_cust_to_onep_cf_payin_list:
    for item in cust_to_onep_cf_list:
        try:
            if item_01 == item.get("referenceId"):
                unmatched_amount_list_2.append(item.get("amount"))
                unmatched_created_at_list_2.append(item.get("paymentTime"))
        except AttributeError:
                pass

# final_unmatched_cid_list = copy.deepcopy(unmatched_cid_list + unmatched_cid_list_2)
# unmatched_cname_list = []
# unmatched_cname_list = extract_company_name(final_unmatched_cid_list)

