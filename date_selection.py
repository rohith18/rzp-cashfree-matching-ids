import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--month", type=str)
parser.add_argument("--day", type=str)
args = parser.parse_args()

# April 1st
base_time_stamp = 1648771200

from_date = base_time_stamp + (int(args.day) - 1)*86400
to_date = from_date + 86400
